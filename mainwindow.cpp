#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
//#include <vector>
#include <QVector>
#define INF 9999

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);
    QObject::connect(ui->MakeTableButton, SIGNAL(clicked()), this, SLOT(MakeTable()));
    QObject::connect(ui->FindPathButton, SIGNAL(clicked()), this, SLOT(FindPath()));
}



void MainWindow::MakeTable()
{
    vertexAmount = ui->VertexAmount->text().toInt();
    ui->GrafTable->setRowCount(vertexAmount);
    ui->GrafTable->setColumnCount(vertexAmount);

        QTableWidget *tableWidget = new QTableWidget(vertexAmount, vertexAmount, this);
    
        for(int row=0; row!=tableWidget->rowCount(); ++row)
        {
            for(int column=0; column!=tableWidget->columnCount(); ++column)
            {
                QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg((row+1)*(column+1)));
                tableWidget->setItem(row, column, newItem);
            }
        }

        for (int i = 0; i!=tableWidget->rowCount(); ++i)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem;
            newItem->setText("0");
            ui->GrafTable->setItem(i,i,newItem);
            ui->GrafTable->item(i,i)->setBackground(Qt::gray);
        }
}


void MainWindow::FindPath()
{
    const int size = vertexAmount;

    int Matrix[size][size];

    for (int i=0; i<size; i++)
        for (int j=i; j<size; j++)
        {
            Matrix[i][j] = ui->GrafTable->item(i,j)->text().toInt();
        }


    for (int i=0; i<size; i++)
        for (int j=i; j<size; j++)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem;
            newItem->setText(QString::number(Matrix[i][j]));
            ui->GrafTable->setItem(j,i,newItem);
        }


    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++)
            Matrix[i][j] = ui->GrafTable->item(i,j)->text().toInt();

    for (int i = 0; i<size; i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem;
        newItem->setText("0");
        ui->GrafTable->item(i,i)->setBackground(Qt::gray);
    }

    ui->SolutionText->clear();

    int end, start,u;

    bool visited[size];
    int distance[size];

    s = ui->StartPoint->text().toInt()-1; start=s+1;
    end = ui->EndPoint->text().toInt()-1;
    for (int i=0; i<size; i++)
    {
        visited[i] = 0;
        distance[i] = INF;
        path[i]=-1;
    }

    distance[s] = 0;

    for (int c=0; c<size-1; c++)
    {
        int min = INF; u=-1;

        for (int i=0; i<size; i++)
            if (!visited[i] && distance[i]<min)
            {
                min=distance[i];
                u=i;
            }

        if (u==-1)
            break;

        visited[u] = 1;

        for (int j=0; j<size; j++)
            if (distance[j] > distance[u] + Matrix[u][j] && Matrix[u][j] > 0)
            {
                distance[j]=distance[u]+Matrix[u][j];
                path[j]=u;
            }
    }

    for (int i=0; i<size; i++)
    {
        if (distance[i]!=INF && i==end)
            ui->SolutionText->addItem(QString::number(start) +" -> " + QString::number(i+1) + "; Вага " + QString::number(distance[i]));
    }

    ui->SolutionText->addItem("Шлях:");

    Out(end);

    ui->SolutionText->addItem(QString::number(end+1));
}

void MainWindow::Out(int v)
{
    if (v == s)
          {
                if (path[v]+1 != 0)
                ui->SolutionText->addItem(QString::number(path[v]+1));
          }
          else
          {
                Out(path[v]);
                if (path[v]+1 != 0)
                ui->SolutionText->addItem(QString::number(path[v]+1));
          }
}


MainWindow::~MainWindow()
{
    delete ui;
}



